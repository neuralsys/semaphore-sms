<?php

namespace Nsid\Semaphore;

class Message
{
    /**
     * Create a new message client instance.
     *
     * @param  \Nsid\Semaphore\Client  @client
     *
     * @return Message
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Send a message to the number.
     *
     * @param string $number
     * @param string $message
     *
     * @return array
     */
    public function send($number, $message)
    {
        $response = $this->client->send('POST', '/sms/send', ['form_params' => [
            'phone' => $number,
            'message' => $message,
            'appId' => $sender = $this->client->getApiName(),
            'accessToken' => $apiKey = $this->client->getApiKey(),
        ]]);

        if (array_key_exists('apiName', $response)) {
            throw new Exceptions\InvalidApiName($apiName);
        }
        if (array_key_exists('apikey', $response)) {
            throw new Exceptions\InvalidApiKey($apiKey);
        }

        if (array_key_exists('number', $response)) {
            throw new Exceptions\InvalidNumber($number);
        }

        return $response;
    }
}
