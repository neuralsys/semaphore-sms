<?php

return [
    'api_name' => env('SEMAPHORE_API_NAME'),
    'api_key' => env('SEMAPHORE_API_KEY'),
    'sender_name' => env('SEMAPHORE_SENDER_NAME'),
];
