<?php

namespace Nsid\Semaphore\Laravel;

use Nsid\Semaphore\Client;

class Facade extends \Illuminate\Support\Facades\Facade
{
    public static function getFacadeAccessor()
    {
        return Client::class;
    }
}
