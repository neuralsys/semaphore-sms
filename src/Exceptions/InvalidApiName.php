<?php

namespace Nsid\Semaphore\Exceptions;

use LogicException;

class InvalidApiName extends LogicException
{
    public function __construct($apiName)
    {
        parent::__construct(
            "The App name [{$apiName}] provided was invalid."
        );
    }
}
