<?php

namespace Nsid\Semaphore;

use GuzzleHttp\Client as GuzzleClient;

class Client
{
    /**
     * The base URL for the Semaphore API.
     *
     * @var string
     */
    const BASE_API = 'http://sms.nsid.online/';

    /**
     * The API name.
     *
     * @var string
     */
    protected $apiName;
    /**
     * The API key.
     *
     * @var string
     */
    protected $apiKey;

    /**
     * The HTTP client.
     *
     * @var \Symfony\HttpClient\HttpClientInterface
     */
    protected $client;

    /**
     * The sender name.
     *
     * @var string
     */
    protected $sender;

    /**
     * Create a new sempahore client instance.
     *
     * @param string $apiKey
     * @param string $sender
     *
     * @return \Nsid\Semaphore\Client
     */
    public function __construct($apiName, $apiKey, $sender = null)
    {
        $this->apiName = $apiName;
        $this->apiKey = $apiKey;
        $this->sender = $sender;
        $this->client = new GuzzleClient(['base_uri' => self::BASE_API,  'timeout' => 2.0]);
    }

    /**
     * Set a different http client.
     *
     * @param \Symfony\HttpClient\HttpClientInterface $client
     *
     * @return void
     */
    public function setHttpClient(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Send a request to the Semaphore API.
     *
     * @param string $method
     * @param string $url
     * @param array  $parameters
     *
     * @return array
     */
    public function send($method, $url, $parameters = [])
    {
        return $response = $this->client->request($method, $url, $parameters);
    }

    /**
     * Get the API key.
     *
     * @return string
     */
    public function getApiName()
    {
        return $this->apiName;
    }

    /**
     * Get the API key.
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Get the sender name.
     *
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Get the messag eclient.
     *
     * @return \Nsid\Semaphore\Message
     */
    public function message()
    {
        return new Message($this);
    }
}
