# Semaphore PHP
This is a PHP client for the [Semaphore] SMS service provider with out of the box Laravel integration.

::: tip
_This library requires a minimum PHP version of 7.1_
:::

## Installation

Install via composer:

```
composer require nsid/semaphore-sms
```

To start using the library, we'll have to provide the Sempahore API key.

## Usage

### Sending a Message

```php
$client = new Client('SEMAPHORE API NAME', 'SEMAPHORE API KEY', 'Sender Name');
$client->message()->send('0917xxxxxxx', 'Your message here');
```